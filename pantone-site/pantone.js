var a = require('./pantoneColorBridgeCoatedV3.json');
var b = require('./pantoneColorBridgeUncoatedV3.json');
var c = require('./pantoneExtendedGamutCoatedM2.json');
var d = require('./pantoneFhCottonTcx.json');
var e = require('./pantoneFhiPaperTpgM2.json');
var f = require('./pantoneMetallicsSolidCoatedM2.json');
var g = require('./pantonePastelsNeonsCoatedM2.json');
var h = require('./pantonePastelsNeonsUncoatedM2.json');
var i = require('./pantoneSkinToneGuideM2.json');
var j = require('./pantoneSolidCoatedV3M2.json');
var k = require('./pantoneSolidUncoatedV3M2.json');

var nu = [a, b, c, d, e, f, g, h, i, j, k];

var fs = require('fs');

fs.writeFile("allpantone.json", JSON.stringify(nu), function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});
